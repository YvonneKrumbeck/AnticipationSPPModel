"""Contains functions to initialise a specific set of positions.

Define a function that returns an array of positions and add it as "pos" mode to 
the main ASPPM class. Mainly used to set up particles for analytic investigation.

Functions
---------
circle_even(n, r, c=[0,0])
    Initialise positions evenly spaced on a circle.
"""

import numpy as np

def circle_even(n, r, c=[0,0]):
    """Initialise positions evenly spaced on a circle.
    
    Parameters
    ----------
    n : int
        Number of particles.
    r : float
        Radius of the circle the particles are intialised on.
    c : array_like, optional
        Coordinates of the centre of the circle. Default is the origin. 
    
    Returns
    -------
    numpy.ndarray
        Evenly spaced positions on a circle.
    """
    angle = 2.*np.pi / n
    x = []
    
    for i in range(n):
        x.append([r*np.cos(i*angle), r*np.sin(i*angle)])
        
    return np.array(x) + c