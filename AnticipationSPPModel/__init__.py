"""A self-propelled particle model that is used to investigate the basic
mechanisms of collective motion with focus on anticipation.

Originally the dynamics are described by a velocity-dependent
acceleration-deceleration term and attraction-repulsion forces.
The module structure of this package allows easy changes of the flow function
that characterises the ODE system and/or the interaction force between
particles. Simply define new functions and, if necessary, adjust the parameter
settings in the respective modules.
Interactively set up and run a simulation with an instance of the ASPPM class.

Example
-------
Import the main class of the model:

>> from AnticipationSPPModel import ASPPM

Create an instance of the class and directly pass optional model parameters:

>> model = ASPPM(tau0=1.)
>> print(model.tau0)
1.0

Alternatively, set parameters after instantiation:

>> model.set_params(n=10, fargs=dict(sp=1., fr=1.))
>> print(model.n)
10
>> print(model.fargs)
{'fr': 1.0, 'margs': {'cr': 2.0, 'ca': 1.0, 'lr': 0.25, 'la': 1.0}, 'sp': 1.0}

Note that the model has to be reinitialised after setting certain parameters!
For instance, the number of particles has been changed to 10, but previously
only 3 positions were initialised:

>> print(model.x.shape)
(3,2)
>> model.initialise()
>> print(model.x.shape)
(10,2)

Run the simulation after the setup:

>> model.iterate()

Retrieve data from the simulation:

>> data = model.data()
>> print(data["y"].shape)
(101,40)

The actual data from solving the ODE system is contained in the variable `y`.
Position and velocity vectors for each time step can be obtained separately:

>> x, v = model._convert_y2xv()
>> print(x.shape)
(101,10,2)
>> print(v.shape)
(101,10,2)
"""

from ._main import ASPPM