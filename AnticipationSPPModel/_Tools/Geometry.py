"""Provides functions for geometric calculations.

Functions
---------
Density(n, d, r)
    Return the particle density on a ball.
Radius(n, d, dens)
    Return the radius of a ball with given particle density.
"""

import numpy as np

def Density(n, d, r):
    """Return the particle density on a ball.
    
    Calculates density in :math:`pi`-units as::
        n / r**2
    
    Parameters
    ----------
    n : int
        Number of particles.
    d : int
        Spacial dimension.
    r : float
        Radius of the ball.
        
    Returns
    -------
    float
        Density in :math:`pi`-units of n particles on a ball with 
        radius r.
    
    Notes
    -----
    Not yet implemented for d>2! Returns 0 instead.
    """
    
    # TODO: Implement general calculation for d>2!
    if d == 2:
        return n * 1. / r**2.
    else:
        print ("WARNING: 'Density' is currently only defined for d=2. " +
               "Returning 0!")
        return 0.
    
def Radius(n, d, dens):
    """Return the radius of a ball with given particle density.
    
    Calculates radius with dens in :math:`pi`-units as::
        sqrt(n / dens)
    
    Parameters
    ----------
    n : int
        Number of particles.
    d : int
        Spacial dimension.
    dens : float
        Particle density. Returns r=1 if set to 0.
        
    Returns
    -------
    float
        Radius of a ball with n particles of density dens.
    
    Notes
    -----
    Not yet implemented for d>2! Returns 1 instead.
    """
    
    # TODO: Implement general calculation for d>2!
    if d == 2:
        if dens:
            return np.sqrt(n*1./dens)
        else:
            return 1.
    else:
        print ("WARNING: `Radius` is currently only defined for `d`=2. " +
               "Returning 1!")
        return 1.
    