"""Provides functions to draw random numbers that require more than 1 
numpy-command.

Functions
---------
Ball(n, d, r)
    Draw random points uniformly distributed on a ball.
"""

import numpy as np

def Ball(n, d, r, c=[0,0]):
    """Draw random points uniformly distributed on a ball.
    
    Parameters
    ----------
    n : int
        Number of points.
    d : int
        Spacial dimension.
    r : float
        Radius of the ball
    
    Returns
    -------
    numpy.ndarray
        Random numbers as array of shape (n,d).
    """
    
    x = []
        
    while len(x) < n:
        tmpX = -r + 2.*r*np.random.rand(d)
        
        if np.linalg.norm(tmpX) <= r:
            x.append(tmpX)
    
    return np.array(x) + c
    
    