"""Provides general functions for convenient operations collected in modules.

Modules
-------
ArrayGymnastics
    Provides numpy.ndarray-operations that require more than 1 numpy-command.
DrawRandom
    Provides functions to draw random numbers that require more than 1 
    numpy-command.
Geometry
    Provides functions for geometric calculations.
"""
