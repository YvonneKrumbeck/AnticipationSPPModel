"""Provides numpy.ndarray-operations that require more than 1 numpy-command.

Functions
---------
Angle(v1, v2, axis=0)
    Return the angle between 2 vectors.
Distance_PBC(v, ls)
    Return the distance vector according to periodic boundary conditions.
DistanceMat(x)
    Return distance vectors of a set of position vectors in matrix-form.
DistanceMat_PBC(x, ls)
    Return distance matrix considering periodic boundary conditions.
MultiplyRows(arr1, arr2)
    Multiply arguments row-wise.
Rotate2D(v, angle)
    Rotate a 2D-vector around a specific angle.
UnitVec(v, axis=0)
    Return the unit vector.    

"""

import numpy as np

def Angle(v1, v2, axis=0):
    """Return the angle between 2 vectors.
    
    
    Parameters
    ----------
    v1,v2 : numpy.ndarray
        Input vectors.
    axis : int, optional
        If v1 and v2 are arrays with more than 1 dimension, specify the 
        axis on which the input vectors lie.
        
    Returns
    -------
    float or numpy.ndarray
        Angle(s) between the input vectors.
    """
    
    ev1 = UnitVec(v1, axis=axis)
    ev2 = UnitVec(v2, axis=axis)
    v1v2 = np.sum(ev1*ev2, axis=axis)
    
    angle = np.arccos(v1v2)
    
    return angle

def Distance_PBC(dx, ls):
    """Return the distance vector according to periodic boundary conditions.
    
    The input vector is recalculated as the shortest distance across all
    lattice boundaries.
    
    Parameters
    ----------
    dx : numpy.ndarray
        Input distance vector(s).
    ls : float
        Side length of the lattice.  
        
    Returns  
    -------
    numpy.ndarray
        Recalculated distance vector as array of shape dx.shape.
    """
    
    return np.remainder(dx+ls/2, ls) - ls/2

def DistanceMat(x):
    """Return distance vectors of a set of position vectors in matrix-form.

    Expands the input array of n position vectors to a row- and column-matrix,
    such that their difference yields an n,n-matrix with the elements
    :math:`dx_{ij}` containing the distance vectors :math:`x_i-x_j`.

    Parameters
    ----------
    x : array_like
        Contains position vectors as an array of shape (n,d), where n is the
        number of positions and d the spatial dimension.

    Returns
    -------
    numpy.ndarray
        n,n-matrix with distance vectors as entries as an array of shape
        (n,n,d).

    Notes
    -----
    This method avoids for-loops over each position vector pair, and thus is
    a lot faster!
    """

    n = len(x)

    rowMat = np.repeat(x[:, None, :], n, axis=1)
    colMat = np.repeat(x[None, :, :], n, axis=0)

    dx = rowMat - colMat

    return dx

def DistanceMat_PBC(x, ls):
    """Return distance matrix considering periodic boundary conditions.
    
    The distance vectors are recalculated as the shortest connection across all
    lattice boundaries.
    
    Parameters
    ----------
    x : array_like
        Contains position vectors as an array of shape (n,d), where n is the
        number of positions and d the spatial dimension.
    ls : float
        Side length of the lattice.  
        
    Returns  
    -------
    numpy.ndarray
        n x n-matrix with shortest distance vectors across all lattice
        boundaries as entries as an array of shape (n,n,d).
    """
    
    return Distance_PBC(DistanceMat(x), ls)

def MultiplyRows(arr1, arr2):
    """Multiply arguments row-wise.

    Each element in row i of arr1 is multiplied with the i-th element of arr2.

    Parameters
    ----------
    arr1 : array_like
        First input array of shape (m,n).
    arr2 : array_like
        Second input array of shape (m,).
        
    Returns
    -------
    numpy.ndarray
        Result of row-wise multiplication of input arrays as array of shape
        arr1.shape

    Notes
    -----
    numpy.multiply only allows element-wise multiplications between
    arrays of shapes (m,n) and (n,)!
    """

    return np.transpose(np.transpose(arr1) * arr2)

def Rotate2D(v, angle):
    """Rotate a 2D-vector around a specific angle.
    
    Parameters
    ----------
    v : array_like
        Input 2D-vector.
    angle : float
        Angle of rotation.
        
    Returns
    -------
    numpy.ndarray
        Rotated 2D-vector.    
    """
    
    rotMat = np.array([[np.cos(angle), -np.sin(angle)],
                       [np.sin(angle),  np.cos(angle)]])
    vr = np.dot(rotMat, v)
    
    return vr

def UnitVec(v, axis=0):
    """Return the unit vector.
    
    Parameters
    ----------
    v : array_like
        Input vector(s).
    axis : int, optional
        If v is an array with more than 1 dimension, specify the axis on which 
        the input vectors lie 
        
    Returns
    -------
    numpy.ndarray
        Normalised input vector(s).
    """
    
    vNorm = np.linalg.norm(v, axis=axis, keepdims=True)
    vNorm = np.where(vNorm == 0, 1., vNorm)
    
    return v/vNorm

if __name__ == "__main__":
    print (__doc__)
