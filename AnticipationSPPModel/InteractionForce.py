"""Contains functions that calculate the interaction force between particles.

Define a function that returns the force f as an array of shape (n,d), which 
then should be called by the flow function to update the particle velocity.

Functions
---------
MorsePotential(x, cr=15., ca=2.5, lr=0.05, la=0.1)
   Return the interaction force based on a general Morse potential.
MorsePotential_PBC(x, ls, cr=15., ca=2.5, lr=0.05, la=0.1)  
    Return the interaction force based on a general Morse potential considering
    periodic boundary conditions.
"""

import numpy as np

from ._Tools.ArrayGymnastics import DistanceMat, DistanceMat_PBC

def MorsePotential(x, cr=15., ca=2.5, lr=0.05, la=0.1): 
    """Return the interaction force based on a general Morse potential.
    
    The Morse potential is defined as ::math::
        U(x) = C_r e^{-|x|/l_r} - C_a e^{-|x|/l_a},
        
    and the resulting force pointing in the direction of x, ::math::
        F = x/|x| (C_r/l_r e^{-|x|/l_r} - C_a/l_a e^{-|x|/l_a}).
        
    The force acting on particle i is calculated depending on the distance 
    between particle i and j and summed over all particles j.        
    
    Parameters
    ----------
    x : array_like
        Array of shape (n,d) containing particle positions.
    cr, ca, lr, la : float
        Parameters of the Morse potential.
    
    Returns 
    -------
    numpy.ndarray
        Array of shape (n,d) contains the respective total force acting on
        particle i.    
    """   
    
    n, d = x.shape
        
    dx = DistanceMat(x)  # Shape (n,n,d)
    xNorm = np.linalg.norm(dx, axis=2, keepdims=True)  # Shape (n,n,1)
    
    repulsiveTerm = cr/lr*np.exp(-xNorm/lr)
    attractiveTerm = ca/la*np.exp(-xNorm/la)
    
    xNorm = np.where(xNorm == 0, 10.**(-6.), xNorm)  # Avoids division by zero.     
    tmpF = 1./xNorm*(repulsiveTerm - attractiveTerm)
    
    f = dx*tmpF        
    fTot = np.sum(f, axis=1)  # Shape (n,d)
    
    return fTot

def MorsePotential_PBC(x, ls, cr=15., ca=2.5, lr=0.05, la=0.1):   
    """Return the interaction force based on a general Morse potential 
    considering periodic boundary conditions.
    
    The Morse potential is defined as ::math::
        U(x) = C_r e^{-|x|/l_r} - C_a e^{-|x|/l_a},
        
    and the resulting force pointing in the direction of x, ::math::
        F = x/|x| (C_r/l_r e^{-|x|/l_r} - C_a/l_a e^{-|x|/l_a}).
        
    The force acting on particle i is calculated depending on the distance 
    between particle i and j, considering periodic boundary conditions, and 
    summed over all particles j.        
    
    Parameters
    ----------
    x : array_like
        Array of shape (n,d) containing particle positions.
    ls : float
        Length of the lattice.
    cr, ca, lr, la : float
        Parameters of the Morse potential.
    
    Returns 
    -------
    numpy.ndarray
        Array of shape (n,d) contains the respective total force acting on
        particle i.    
    """
     
    n, d = x.shape
        
    dx = DistanceMat_PBC(x,ls)   # Shape (n,n,d)
    xNorm = np.linalg.norm(dx, axis=2, keepdims=True)  # Shape (n,n,1)
    
    repulsiveTerm = cr/lr*np.exp(-xNorm/lr)
    attractiveTerm = ca/la*np.exp(-xNorm/la)
    
    xNorm = np.where(xNorm == 0, 10.**(-6.), xNorm)  # Avoids division by zero. 
    tmpF = 1./xNorm*(repulsiveTerm - attractiveTerm)
    
    f = dx*tmpF
    
    fTot = np.sum(f, axis=1)    # Shape (n,d)
    
    return fTot
