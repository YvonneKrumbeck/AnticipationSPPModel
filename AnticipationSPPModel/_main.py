"""Contains the main class definition of the SPP model."""

import numpy as np
from scipy import integrate

from .FlowFunction import f, fargs
from .Termination import none
from ._Tools import DrawRandom
from ._Tools.Geometry import Density, Radius
from . import InitPositions

class ASPPM(object):
    """An anticipation-based self-propelled particle model.
    
    Interactively set up and run a simulation with an instance of this class.
    Upon instantiation, a simulation is directly initialised with the default
    parameters if not specified. 
    
    Parameters
    ----------
    **params
        Specify model parameters as keyword arguments. Default values are used
        otherwise (see ``__init__``).
    
    Attributes
    ----------
    n : int
        Number of particles.
    d : int
        Spatial dimension.
    ls : float
        Side length of the lattice (mainly needed for periodic boundary
        conditions).
    r : float
        Radius of the ball/circle that particles are initialised in when
        the position mode is set to "ball" or "circleEven".
    dens : float
        Initial particle density corresponding to `n` and `r`. If specified,
        the radius `r` is adjusted accordingly.
    v0 : float or 1D array
        Initial velocity of the particles. 
    m0 : float
        Mass of the particles.
    tau0 : float
        Anticipation time of the particles.
    t0 : float
        Starting time.
    tMax : float
        Maximum time.
    dt : float
        Time step at which the integrator is called and data recorded.
    maxStep : float
        Maximum step size of internal integration steps passed to `dopri5`
        integrator.
    f : callable f(t, y, *args)
        Flow function that describes the ODE system and is passed to the
        integrator.
    fargs : dict
        Additional parameters passed to f. Note that the true input arguments
        might be modified (see _modify_fargs method)!
    modes : dict
        Initialisation modes for position ("pos"), velocity ("vel"), mass
        ("mass") and anticipation time ("tau") of the particles. See respective
        `_init` methods for details.
    termFunc : callable termFunc(y[-nStepsTerm:], *args) -> bool
        Specify a function that returns True if the simulation
        should be terminated before the maximum time `tMax` is reached.
    nStepsTerm : int
        Number of the last solutions recorded from the integrator for the
        evaluation of the termination function `termFunc`.
    x : numpy.ndarray
        Array of size (n,d) contains the positions of all particles. Use the 
        `_convert_y2xv` method to update the values and get a position array
        for all times `t`. 
    v : numpy.ndarray
        Array of size (n,d) contains the velocities of all particles. Use the 
        `_convert_y2xv` method to update the values and get a velocity array
        for all times `t`.
    m : numpy.ndarray
        Array of size (n,) contains the masses of all particles.
    tau : numpy.ndarray
        Array of size (n,) contains the anticipation times of all particles.
    y : numpy.ndarray
        Array of size (len(t),nxd) that stores the positions and velocities
        of all particles that are recorded at times t. 
        y[0] = [x11, ..., x1d, x21, ..., xnd, v11, ..., v1d, v21 ..., vnd]
    t : numpy.ndarray
        Array that stores all times where data is recorded.
    odeSolver : instance of scipy.integrate.ode class
        See `_init_odeSolver` for details.
        
    Methods
    -------
    set_params(**params)
        Set parameters for the simulation.
    initialise()
        Initialise/Reset the simulation.
    iterate()
        Start the simulation.
    data()
        Return data of the simulation.
    """   
    
    def __init__(self, **params):        
        self.n = 3
        self.d = 2
        self.ls = 10.
        self.r = 1.
        self.dens = Density(self.n, self.d, self.r)
        
        self.v0 = 0.
        self.m0 = 1.
        self.tau0 = 0.
        
        self.t0 = 0.
        self.tMax = 100.
        self.dt = 1. 
        self.maxStep = 0.01 
        
        self.f = f
        self.fargs = fargs
        
        self.modes = dict(pos="lattice",
                          vel="same",
                          mass="same",
                          tau="same")
        
        self.termFunc = none
        self.nStepsTerm = 50
        
        self.set_params(**params)
        self.initialise()
        
    #-------------------------------------------------------------------------- 
    
    def set_params(self, 
                   n=None, d=None, ls=None, r=None, dens=None,
                   v0=None, m0=None, tau0=None,
                   t0=None, tMax=None, dt=None, maxStep=None,
                   f=None, fargs=dict(), modes=dict(), 
                   termFunc=None, nStepsTerm=None):
        """Set parameters for the simulation.
        
        Parameters that are specified as keyword argument will be set
        accordingly. Note that the `initialise` method has to be called after
        setting some of the parameters in order to have an effect on the
        simulation.
        """
        
        if n is not None:
            self.n = n
            self.dens = Density(self.n, self.d, self.r)
        if d is not None:
            self.d = d
        if ls is not None:
            self.ls = ls
        if r is not None:
            self.r = r
            self.dens = Density(self.n, self.d, self.r)
        if dens is not None:
            self.dens = dens
            self.r = Radius(self.n, self.d, self.dens)
            
        if v0 is not None:
            self.v0 = v0
        if m0 is not None:
            self.m0 = m0
        if tau0 is not None:
            self.tau0 = tau0
        
        if t0 is not None:
            self.t0 = t0
        if tMax is not None:
            self.tMax = tMax
        if dt is not None:
            self.dt = dt
        if maxStep is not None:
            self.maxStep = maxStep    
        
        if f is not None:
            self.f = f
        if termFunc is not None:
            self.termFunc = termFunc
        if nStepsTerm is not None:
            self.nStepsTerm = nStepsTerm
        
        self.fargs.update(fargs)         
        self.modes.update(modes)
    
    #-------------------------------------------------------------------------- 
    
    def _init_x(self, x0=None):
        """Initialise positions x.
        
        If the `modes` value for "pos" is set to
            -"ball", positions are initialised randomly and uniformly
                distributed inside a ball with radius r.
            -"lattice", positions are initialised randomly and uniformly
                distributed inside the lattice.
            -"circleEven", positions are initialised evenly spaced on a circle
                with radius r.
        """
        
        if self.modes["pos"] == "ball":
            center = self.ls/2.
            self.x = DrawRandom.Ball(self.n, self.d, self.r, [center,center])
        if self.modes["pos"] == "lattice":
            self.x = self.ls*np.random.random((self.n, self.d))
        if self.modes["pos"] == "circleEven":
            center = self.ls/2.
            self.x = InitPositions.circle_even(self.n, self.r, [center,center])
    
    def _init_v(self):
        """Initialise velocities v.
        
        If the `modes` value for "vel" is set to
            -"same", all initial velocities are set to v0 (float).
            -"input", velocities are set to the n,d-array v0.
        """
        
        if self.modes["vel"] == "same":
            self.v = self.v0 * np.ones((self.n, self.d))
        if self.modes["vel"] == "input":
            self.v = self.v0
            
    def _init_m(self):
        """Initialise masses m.
        
        If the `modes` value for "mass" is set to
            -"same", all masses are set to m0 (float).
        """
        
        if self.modes["mass"] == "same":
            self.m = self.m0 * np.ones(self.n)
            
    def _init_tau(self):
        """Initialise anticipation times tau.
        
        If the `modes` value for "tau" is set to
            -"same", all anticipation times are set to tau0 (float).
        """
        
        if self.modes["tau"] == "same":
            self.tau = self.tau0 * np.ones(self.n)
    
    def _init_odeSolver(self):
        """Initialise integrator for solving the ODE system.
        
        Creates an instance of `scipy.integrate.ode` class. Takes the flow 
        function f as argument. The integrator is set to "dopri5" (see SciPy
        documentation for details). The initial values are set to a flattened
        array of size (2*n*d) that contains the positions and velocities of all 
        particles and t0 is passed as initial time.
        """
        
        y0 = self._convert_xv2y()
        
        # Internal integration step size should not exceed sampling step.
        if self.maxStep > self.dt:
            self.maxStep = self.dt
        
        self.odeSolver = integrate.ode(self.f)
        self.odeSolver.set_integrator("dopri5", nsteps=10**4, max_step=self.maxStep)
        self.odeSolver.set_initial_value(y0, self.t0)
        
        # If the arguments need to be updated during time (e.g. time-dependent
        #  tau), put following modification inside the iterate loop!
        tmpFargs = self._modify_fargs()
        self.odeSolver.set_f_params(*tmpFargs)
        
    def _modify_fargs(self):
        """Return modified function arguments.
        
        Allows passing simulation parameters and initialised values to the
        flow function.
        Also, m and tau are converted to arrays of size (n,d) for faster and
        more convenient array operations with x and v.
        
        Returns
        -------
        Tuple
            Flow function arguments.
            
        Notes
        -----
        Depending on the definition of the flow function this methods might need
        to be adjusted accordingly!
        """        
        
        tmpM = np.repeat(self.m, self.d)
        tmpTau = np.repeat(self.tau, self.d)
        tmpFargs = (self.n, self.d, tmpM, tmpTau, self.ls, self.fargs) 
        
        return tmpFargs        
    
    def initialise(self):
        """Initialise/Reset the simulation.
        
        Positions, velocities, masses and anticipation times are initialised
        for all n particles, as well as the integrator for solving the ODE
        system. See respective `_init` methods for details.
        """
        
        self._init_x()
        self._init_v()
        self._init_m()
        self._init_tau()
        self._init_odeSolver()
    
    #-------------------------------------------------------------------------- 
    
    def iterate(self):
        """Start the simulation.
        
        Iterate until the maximum time is reach or the simulation is terminated.
        Every iteration step the integrator is called and data recorded for that
        step and stored as arrays y and t.
        """
        
        self.y = [self.odeSolver.y]
        self.t = [self.odeSolver.t]
        
        while self.odeSolver.successful() and (self.t[-1] < self.tMax):
            self.odeSolver.integrate(self.odeSolver.t + self.dt)
            self.y.append(self.odeSolver.y)
            self.t.append(self.odeSolver.t)
            
            if self.termFunc(self.y[-self.nStepsTerm:], self.n, self.d, self.m, self.fargs):
                break
            
        self.y = np.array(self.y)
        self.t = np.array(self.t)
            
    def _convert_xv2y(self):
        """Convert position and velocity arrays to a single flat array.
        
        The position and velocity arrays x and v are concatenated to a
        flattened array y to be passed to the ODE solver function::        
            y = [x11, ..., x1d, x21, ..., xnd, v11, ..., v1d, v21 ..., vnd]
        """
        
        return np.concatenate((self.x, self.v)).ravel()
    
    def _convert_y2xv(self):
        """Convert the data array `y` to separate position and velocity arrays.
        
        Sets the position and velocity arrays `x` and `v` to the current values
        and returns all recorded data separately as arrays of size (nt,n,d).
        
        Returns
        -------
        numpy.ndarray
            Array of size (nt,n,d) contains the positions of all particles that
            have been recorded at times t.
        numpy.ndarray
            Array of size (nt,n,d) contains the velocities of all particles that
            have been recorded at times t.                
        """
        
        x = []
        v = []
        
        for yt in self.y:
            ny = len(yt)
            x.append(yt[:ny//2].reshape(self.n, self.d))
            v.append(yt[ny//2:].reshape(self.n, self.d))
        
        self.x = x[-1]
        self.v = v[-1]
            
        return np.array(x), np.array(v)  
    
    def data(self):
        """Return data of the simulation.
        
        The simulation parameters and recorded data are returned in form of a 
        dictionary containing all attributes of the class instance.
        
        Returns
        -------
        dict
            Copy of the `__dict__` attribute with modifications.
        """
        
        d = self.__dict__.copy()        
        d["f"] = self.f.__name__
        d["termFunc"] = self.termFunc.__name__
        d["success"] = self.odeSolver.successful()
        
        return d      
