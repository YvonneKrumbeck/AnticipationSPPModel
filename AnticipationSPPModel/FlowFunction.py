"""Contains the flow function that characterises the ODE system.

Define a function f(t, y, *args), that characterises the ODE system
:math:`y'(t)=f(t,y)`. The array y should contain the coordinates of the position
and velocity vectors of all particles.
Set the default function and parameters as f and fargs, which are automatically
imported and set by the main class of the simulation. 

Functions
---------
anticipatedPos(x, v, tau)
    Return anticipated positions.
morse_velTerm_PBC(t, y, n, d, m, tau, ls, args)
    Flow function of an ODE system based on the Morse potential and an 
    acceleration/deceleration term considering periodic boundary conditions.
    
Notes
-----
Passing parameters to the flow function might be adjusted in the main class of 
the simulation (see _modify_fargs method).
"""

import numpy as np

from . import InteractionForce

def anticipatedPos(x, v, tau):
    """Return anticipated positions.

    Parameters
    ----------
    x,v : numpy.ndarray
        Positions/velocities array of shape (n,d).
    tau : numpy.ndarray
        Anticipation times array of shape (n,).
        
    Returns
    -------
    numpy.ndarray
        Extrapolated positions array of shape (n,d).
    """

    return x + v*tau

def morse_velTerm_PBC(t, y, n, d, m, tau, ls, args):
    """Flow function of an ODE system based on the Morse potential and an 
    acceleration/deceleration term considering periodic boundary conditions.
    
    The flow function characterises the following ODE system ::math::
        dx_i/dt = v_i
        dv_i/dt = sum(F_{ij}) + (\alpha - \beta |v_i|^2) v_i 
    
    Parameters
    ----------
    t : float
        Time where flow function is evaluated. Required positional argument for
        the integrator, although f is not explicitly time dependent.
    y : array_like
        1D array of shape (2*n*d) containing the input data, i.e.::
            y = [x11, ..., x1d, x21, ..., xnd, v11, ..., v1d, v21 ..., vnd]
    n : int
        Number of particles.
    d : int
        Spatial dimension.
    m,tau : array_like
        Array of shape (n,d) containing the masses/anticipation times of all 
        particles. 2D array allows more convenient calculations with the 
        position and velocity vectors. 
    ls : float
        Side length of the lattice.
    args : dict
        Specific function parameters:: 
            sp : self-propulsion (alpha), 
            fr : friction (beta),
            margs : Morse parameters dict(cr, ca, lr, la) (see MorsePotential in
            InteractionForce module) 
    """
       
    ny = len(y)
    dy = np.zeros(ny)   
    
    # Update positions 
    dy[:ny//2] = y[ny//2:]    
    np.remainder(y[:ny//2], ls, out=y[:ny//2])
    
    # Interaction force (Morse)
    xa = anticipatedPos(y[:ny//2], y[ny//2:], tau)
    xa = xa.reshape(n,d)
    f = InteractionForce.MorsePotential_PBC(xa, ls, **args["margs"])
    
    # Velocity dependent term
    v = y[ny//2:]
    v = v.reshape(n,d)
    vv = np.linalg.norm(v, axis=1) **2.
    vv = np.reshape(vv, (n,1)) 
    velTerm = (args["sp"] - args["fr"]*vv)*v
    
    # Update velocities
    dy[ny//2:] = 1./m * (velTerm.ravel() + f.ravel())
    
    return dy

# Default
f = morse_velTerm_PBC
fargs = dict(sp=0., fr=0.,
             margs=dict(cr=2., ca=1., lr=0.25, la=1.))
    
    
    