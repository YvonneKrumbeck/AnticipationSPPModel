"""Contains functions to terminate simulations prematurely.

Define a function that returns True if the condition to terminate a simulation
is met and False otherwise.

Functions
---------
none(*args)
    Return False, i.e. no premature termination. (default)
groupFormed(y, n, d, m, args)
    Return True if the polarisation or rotation exceed a threshold.
polarisation(y, n, d, m, args)
    Return True if the polarisation exceeds a threshold.
"""

import numpy as np

from .Analysis import Polarisation, Rotation, ScaledSize

def none(*args):
    """Return False, i.e. no premature termination. (default)"""
    
    return False

def groupFormed(y, n, d, m, args):
    """Return True if the polarisation or rotation exceed a threshold.
    
    Parameters
    ----------
    y : numpy.ndarray
        Input data of shape (nt,2*n*d)
    n : int
        Number of particles.
    d : int
        Spatial dimension.
    m : numpy.ndarray
        Mass of n particles.
    args : dict
        Contains additional arguments.
    
    Returns
    -------
    bool
        True if the mean polarisation or rotation exceed a threshold and False 
        otherwise.    
    """
    
    y = np.array(y)
    nt = len(y)
    ny = len(y[0])
    
    x = y[:,:ny//2].reshape((nt, n, d))
    v = y[:,ny//2:].reshape((nt, n, d))
    
    p = Polarisation(v)    
    if np.mean(p) > 0.99:
        return True
    
    r = Rotation(x, v, m, args["ls"])
    if np.mean(r) > 0.99:
        return True
    
    '''
    s = ScaledSize(x, args["ls"])
    if np.mean(s) < 0.01:
        return True
    '''
    
    return False    
    
def polarisation(y, n, d, m, args):
    """Return True if the polarisation exceeds a threshold.
    
    Parameters
    ----------
    y : numpy.ndarray
        Input data of shape (nt,2*n*d)
    n : int
        Number of particles.
    d : int
        Spatial dimension.
    m : numpy.ndarray
        Mass of n particles.
    args : dict
        Contains additional arguments.
    
    Returns
    -------
    bool
        True if the mean polarisation exceeds a threshold and False otherwise.    
    """
    y = np.array(y)
    nt = len(y)
    ny = len(y[0])
    
    x = y[:,:ny//2].reshape((nt, n, d))
    v = y[:,ny//2:].reshape((nt, n, d))
    
    p = Polarisation(v)    
    if np.mean(p) > 0.99:
        return True
    else:
        return False
    
    
    