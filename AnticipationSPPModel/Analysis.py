"""Contains functions to analyse the output data of simulations.

Functions
---------
CentreOfMass(x, m)
    Return the centre of mass of a set of particles.
CentreOfMass_PBC(x, m, ls)
    Return the centre of mass considering periodic boundary conditions.
Polarisation(v)
    Return the polarisation according to the input vectors.
Rotation(x, v, m, ls)
    Return the rotation of the input data considering periodic boundary 
    conditions.
ScaledSize(x, ls)
    Return the scaled size according to the input data considering periodic
    boundary conditions.
TimeScale(cr, ca, lr, la, sp, fr)
    Return the characteristic time scale of the system.
"""

import numpy as np

from ._Tools.ArrayGymnastics import Distance_PBC, DistanceMat_PBC, UnitVec

def CentreOfMass(x, m):
    """Return the centre of mass of a set of particles.
    
    Parameters
    ----------
    x : array_like
        Array of shape (nt,n,d) containing the positions of the particles.
    m : array_like
        Masses of the particles.
        
    Returns
    -------
    numpy.ndarray
        Array of shape (nt,d) containing the coordinates of the centre of mass
        at times t.
    """
    
    mTot = np.sum(m)
    com = np.zeros_like(x[:,0,:])
    
    for d in range(x.shape[2]):
        com[:,d] = np.sum(x[:,:,d]*m, axis=1) / mTot
    
    return com

def CentreOfMass_PBC(x, m, ls):
    """Return the centre of mass considering periodic boundary conditions.
    
    Treats the input coordinates as if they were on a circle, i.e. maps them to 
    an angle theta and generates two new coordinates for each circle
    accordingly, weighted with the particle mass. New angles are obtained from
    the averages and mapped back to the initial system.
    
    Parameters
    ----------
    x : numpy.ndarray
        Array of shape (nt,n,d) containing the positions of the particles.
    m : array_like
        Masses of the particles.
    ls : float
        Side length of the lattice.
        
    Returns
    -------
    numpy.ndarray
        Array of shape (nt,d) containing the coordinates of the centre of mass
        at times t considering periodic boundary conditions.
    """
    
    # Obtain weighted coordinates on circles 
    theta = x * 2.*np.pi/ls
    xp1 = np.zeros_like(theta)
    xp2 = np.zeros_like(theta)
    
    for d in range(x.shape[2]):
        xp1[:,:,d] = np.cos(theta[:,:,d]) * m
        xp2[:,:,d] = np.sin(theta[:,:,d]) * m
    
    xp1 = np.mean(xp1, axis=1)
    xp2 = np.mean(xp2, axis=1)
    
    # Calculate centre of mass from new angles mapped back
    theta = np.arctan2(-xp2, -xp1) + np.pi
    com = np.zeros_like(x[:,0,:]) 
    com = theta * ls/(2.*np.pi)
    
    return com    
    
def Polarisation(v):
    """Return the polarisation according to the input vectors.
    
    Calculates the polarisation as the absolute mean normalised velocity.
    
    Parameters
    ----------
    v : numpy.ndarray
        Array of shape (nt,n,d) containing the velocity vectors.
        
    Returns
    -------
    numpy.ndarray
        Array of shape (nt,) containing the polarisation values for all times t.    
    """
    
    tmpP = UnitVec(v, axis=2)
    tmpP = np.sum(tmpP, axis=1)
    tmpP = np.linalg.norm(tmpP, axis=1)
    
    p = tmpP / len(v[0])
    
    return p

def Rotation(x, v, m, ls):
    """Return the rotation of the input data considering periodic boundary 
    conditions.
    
    Calculates the rotation as the absolute mean normalised angular momentum.
    
    Parameters
    ----------
    x,v : numpy.ndarray
        Array of shape (nt,n,d) containing the position/velocity vectors.
    m : numpy.ndarray
        Array of shape (n,) containing the masses of the particles.
    ls : float
        Side length of the lattice.
        
    Returns
    -------
    numpy.ndarray
        Array of shape (nt,) containing the rotation values for all times t.
    
    Notes
    -----
    Only implemented for 2D!    
    """
    
    # Get distance between x and centre of mass
    com = CentreOfMass_PBC(x, m, ls)
    a = x - com[:,None,:] 
    a = Distance_PBC(a, ls)
    
    a = UnitVec(a, axis=2)
    v = UnitVec(v, axis=2)   
    
    tmpR = np.cross(v, a, axis=2)
    tmpR = np.sum(tmpR, axis=1)
    tmpR = np.abs(tmpR)
    
    r = tmpR / len(v[0])
    
    return r

def ScaledSize(x, ls):
    """Return the scaled size according to the input data considering periodic
    boundary conditions.
    
    Calculates the scaled group size as the smallest square that contains all
    particle positions across boundaries.
    
    Parameters
    ----------
    x : numpy.ndarray
        Array of shape (nt,n,d) containing the position vectors.
    ls : float
        Side length of the lattice.
        
    Returns
    -------
    numpy.ndarray
        Array of shape (nt,) containing the scaled size values for all times t.
    """
    
    s = []
    
    for xt in x:
        dx = DistanceMat_Periodic(xt, ls)
        lx = 1.
        
        for d in range(dx.shape[2]):
            lx *= np.max(dx[:,:,d])
            
        s.append(lx/(ls/2)**2)
    
    return s

def TimeScale(cr, ca, lr, la, sp, fr):
    """Return the characteristic time scale of the system.
    
    Calculates the time scale based on the equilibrium distance between 
    particles, according to the interaction potential, and their characteristic 
    speed :math:`v_0=sqrt(alpha/beta)`.
    
    Parameters
    ----------
    cr,ca,lr,la : float
        Parameters of the Morse potential.
    sp,fr : float
        Self-propulsion/Friction parameter of the velocity dependent term.
        
    Returns
    -------
    float
        The characteristic time scale.
    """
    
    rc = la*lr / (la-lr) * np.log((cr/ca) / (lr/la))
    vc = np.sqrt(sp/fr)
    tc = rc/(vc + 10**(-6))
    
    return tc
    
        
    
    