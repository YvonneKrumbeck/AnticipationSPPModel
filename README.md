# Anticipation self-propelled particle model

A model that is used to investigate the basic mechanisms of collective motion
with focus on anticipation.
In particular, individuals possess predictive abilities and base their motion on 
a system state a few time steps ahead.
Here, $`\tau`$ denotes the anticipation time.

Originally, the dynamics are described by a velocity-dependent
acceleration-deceleration term and attraction-repulsion forces,

```math
\dot{\vec{x}}_i
= \vec{v}_i,
```
```math
m_i \dot{\vec{v}}_i 
= (\alpha - \beta |\vec{v}_i|^2) \vec{v}_i
- \sum_{j=1,\, j \ne i}^N \vec{\nabla} U(\vec{x}_i^{\tau} - \vec{x}_j^{\tau}),
```

where $`\vec{x}_i`$, $`\vec{v}_i`$ and $`m_i`$ are the position, velocity and mass
of particle $`i=1,...,N`$ respectively.
$`\alpha`$ and $`\beta`$ are the self-propulsion and friction parameters that
define the characteristic speed of the particles.
The last term describes pairwise particle interactions via a general potential
$`U(\vec{x})`$.

The module structure of this package allows easy changes of the flow function 
that characterises the ODE system and/or the interaction force between
particles.
Simply define new functions and, if necessary, adjust the parameter settings in 
the respective modules.

Interactively set up and run simulations as demonstrated in 
`ExampleSimulation.html`.

### Requirements

- Python 2.7 or 3
- NumPy
- SciPy
